$(document).ready(function(){
	
	$('.resultFilter').on('click', '.pagess', function(e){
		
		e.preventDefault();
		
		var numbers = $(this).attr('numbers');
		var material = $('.material').val();
		var kategoriya_lota_id = $('.kategoriya_lota_id').val();
		var status_lota_id = $('.status_lota_id:checked').val();
		var regiony_id = $('.regiony_id').val();
		
		$(".resultFilter").load(
		  "/ajax/filtergrid.php",
		  {
			material: material,
			kategoriya_lota_id: kategoriya_lota_id,
			status_lota_id: status_lota_id,
			regiony_id: regiony_id,
			numbers: numbers
		  });	
		
	});
	
	$('.filter_postav').on('click', '.win', function(e){
		
		e.preventDefault();
		
		var otvet_na_zayavku_id = $(this).attr('otvet_na_zayavku_id');
		
		var kommentarii_zak = prompt('Ваш комментарий?', '');
		
		if(kommentarii_zak != null)
		{
			$.post(
				  "/ajax/otvet_na_zayavku.php",
				  {
					otvet_na_zayavku_id: otvet_na_zayavku_id,
					kommentarii_zak: kommentarii_zak
				  },
				  function(result)
				  {
					 alert(result.message);
					 
					 $('.win').hide();
				  }
				);
		}
	});
	
	
	$(".select_record").click(function(e){
		
		e.preventDefault();
		
		var zayavka_id = $(this).parent().attr('select_record');
		
		$(".select_record").parent().removeClass('activeTable');
		
		$(this).parent().addClass('activeTable');
		
		
		$(".filter_postav").load("/ajax/uchastniki_lota.php",{
						zayavka_id: zayavka_id});
						
						
						
		
			$('html, body').animate({
				scrollTop: $("#lot_history").offset().top
			}, 500);
		
		
		
	});
	
	
	$('.resultFilter').on('click', '.izbrannoe', function(e){
		
		e.preventDefault();
		
		knopka = $(this);
		
		var ident = knopka.parent().parent().children('.ident').val();
		
		var yes = confirm('Добавить лот в избранное?');
		
		if(yes)
		{
			$.post(
			  "/ajax/v_izbrannoe.php",
			  {
				ident: ident
			  },
			  onAjaxSuccess
			);
			 
			
		}
		
	});
	
	function onAjaxSuccess(data)
			{
				//alert(data.message);
				knopka.parent().html(data.knop);
			}
			
			
		$('.resultFilter').on('click', '.notizbrannoe', function(e){
		
		e.preventDefault();
		
		knopka = $(this);
		
		var ident = knopka.parent().parent().children('.ident').val();
		
		var yes = confirm('Убрать лот из избранного?');
		
		if(yes)
		{
			$.post(
			  "/ajax/iz_izbrannogo.php",
			  {
				ident: ident
			  },
			  onAjaxNotSuccess
			);
			 
			
		}
		
	});
	
	function onAjaxNotSuccess(data)
			{
				//alert(data.message);
				knopka.parent().html(data.knop);
			}		

	
	$('.contry').change(function(){
		
		var contry = $(this).val();
		
		$(".region").load(
		  "/ajax/filterregion.php",
		  {
			contry: contry
		  });
		
	});
	
	$('.cifri').on('keyup keypress', function(e) {
		   if (e.keyCode == 8 || e.keyCode == 46) {}
		   else
			 {
			   var letters='1234567890.';
							return (letters.indexOf(String.fromCharCode(e.which))!=-1);
						}
    });
	
	
/*http://api.jqueryui.com/datepicker/  */
				
				
	$( "#datepicker1" ).datepicker({
	  dateFormat: 'dd.mm.yy',
      changeMonth: true,
      changeYear: true
    });
				
				
				
	$( "#datepicker2" ).datepicker({
	  dateFormat: 'dd.mm.yy',
      changeMonth: true,
      changeYear: true
    });
				

	$('.filterClear').click(function(e){
		
		e.preventDefault();
		
		$('.formfilter')[0].reset();
		
	});
	
	
	
	$('.filerAj').click(function(e){
		
		e.preventDefault();
		
		var material = $('.material').val();
		var kategoriya_lota_id = $('.kategoriya_lota_id').val();
		
		
		
		var status_lota_id = $('.status_lota_id:checked').val();
		
		
		
		
		
		var regiony_id = $('.regiony_id').val();
		
		$(".resultFilter").load(
		  "/ajax/filtergrid.php",
		  {
			material: material,
			kategoriya_lota_id: kategoriya_lota_id,
			status_lota_id: status_lota_id,
			regiony_id: regiony_id,
		  });
	
		
	});
	
		$(".formloginvalidate").validate({
		submitHandler: function(form) {
			
			var m_data=$('.formloginvalidate').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/avtorization.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
					document.location.href = "/";
					// window.location.reload();
					 
					
				}
				else
				{
					$('.neuspeh').html(result.message);
				}
				
			}
			});
			
			
		},
       rules:{
			login:{
				digits: true,
				required: true,
				minlength: 12,
                maxlength: 12,	
				
			},
        
		password:{
				required: true,
  
            },

       },
       messages:{


			
			 login:{
                digits:    "ИИН/БИН состоит исключительно из цифр",
				required:  "Введите ИИН/БИН организации",
				minlength: "Длина ИИН/БИН не должна быть меньше 12 символов",
                maxlength: "Длина ИИН/БИН не должна быть больше 12 символов",
            },

			password:{
                required:  "Пароль не может быть пустым",
      
				
            }
        }
	 		
    });	
	
	$(".formfeatbak").validate({
		submitHandler: function(form) {
			
			var m_data=$('.formfeatbak').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/contaktform.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
					$('.formfeatbak').slideUp(1000);
					$('.uspeh').html(result.message);
				}
				else
				{
					$('.neuspeh').html(result.message);
				}
				
			}
			});
			
			
		},
       rules:{
			name:{
				required: true,
				
			},
            email:{
				required: true,
                email: true,
            },
			
			phone:{
				required: true,
				digits:true,

			},

			message:{
				required: true,
                minlength: 6,
                maxlength: 500,
				
            }
       },
       messages:{

			name:{
                required: "Введите ваше имя"
            },
	   
            email:{
                email: "Введите корректный E-mail",
				required:  "Введите ваш E-mail" 
            },
			
			 phone:{
                digits: "Поле телефон является цифровым полем",
				required:  "Введите ваш телефон",

            },

			message:{
                required:  "Введите сообщение",
                minlength: "Длина сообщения не должна быть меньше 6 символов",
                maxlength: "Длина сообщения не должна быть больше 500 символов",
				
            }
        }
	 		
    });
	
	
	
	//Проверка формы регистрации
	$(".register_lot").validate({
		submitHandler: function(form) {
			
			var m_data=$('.register_lot').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/register_lot.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
					$('.register_lot').slideUp(1000);
					$('.uspeh').html(result.message);
					$('.neuspeh').html('');
				}
				else
				{
					$('.neuspeh').html(result.message);
				}
			}
			});

		},
       rules:{
		   
			kategoriya_lota_id:{
				required: true,
			},
			
            material_v_lote:{
				required: true,
            },
			
		
			
			edinitsa_izmereniya_id:{
				required: true,
            },
			
			priobretaemoe_kolichestvo:{
				required: true,
				minlength: 1,
            },
			
			data_otkrytiya_zakupki:{
				required: true,
            },
			
			data_zakrytiya_zakupki:{
				required: true,
            },
			
			mesto_postavki:{
				required: true,
            },
			
				
			otvetstvennoe_litso_za_zakupku:{
				required: true,
			},

		
       },
	   
       messages:{
		   
			kategoriya_lota_id:{
                required:  "Выберите категорию лота",
            }, 
		   
		   	material_v_lote:{
                required:  "Введите приобретаемый товар/работу/услугу",
            }, 
			
	
			edinitsa_izmereniya_id:{
                required:  "Выберите единицу измерения",
            }, 
			
			priobretaemoe_kolichestvo:{
                required:  "Введите приобретаемое количество товара/работы/услуги",
                minlength: "Длина  не должна быть меньше 1 символа",	
            }, 

			data_otkrytiya_zakupki:{
                required:  "Введите дату публикации лота",
            }, 		

			data_zakrytiya_zakupki:{
                required:  "Введите дату завершения лота",
            }, 	

			mesto_postavki:{
                required:  "Введите место поставки товара/работы/услуги",
            }, 
			
			otvetstvennoe_litso_za_zakupku:{
                required:  "Введите ответственное лицо за закупку и его номер телефона",
            }, 

		   
       }
	   
		
    });
	
	

	
	//Проверка формы регистрации
	$(".register_organizatsiya").validate({
		submitHandler: function(form) {
			
			var m_data=$('.register_organizatsiya').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/register_organizatsiya.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
					$('.register_organizatsiya').slideUp(1000);
					$('.uspeh').html(result.message);
					$('.neuspeh').html('');
				}
				else
				{
					$('.neuspeh').html(result.message);
				}
			}
			});

		},
       rules:{
		   
			iin:{
				required: true,
				digits:true,
				minlength: 12,
                maxlength: 12,	
			},
			
            organizatsiya:{
				required: true,
            },
			
		
			familiya:{
				required: true,
            },
			
			kod_mobilmznogo_operatora:{
				required: true,
				digits:true,
				minlength: 3,
                maxlength: 3,	
            },
			
			telefon:{
				required: true,
				digits:true,
				minlength: 7,
                maxlength: 7,				
            },
			

			elektronnyy_yashchik:{
				required: true,
                email:true,
            },
			
			parol: "required",
			paroltwo: {
			equalTo: "#password"
			}
       },
	   
       messages:{
		   
			iin:{
                required:  "Введите БИН/ИИН организации",
                minlength: "Длина сообщения не должна быть меньше 12 символов",
                maxlength: "Длина сообщения не должна быть больше 12 символов",

            }, 
		   
		   	organizatsiya:{
                required:  "Введите название организации",
            }, 
			
			
			familiya:{
                required:  "Введите ФИО контактного лица",
            }, 
			
			kod_mobilmznogo_operatora:{
                required:  "Введите код мобильного оператора, например 777,705,701 и т.д.",
                minlength: "Длина кода не должна быть меньше 3 символов",
                maxlength: "Длина кода не должна быть больше 3 символов",
            }, 
			
			telefon:{
                required:  "Введите телефон контактного лица",
                minlength: "Длина телефона не должна быть меньше 7 символов",
                maxlength: "Длина телефона не должна быть больше 7 символов",
            }, 
			
			elektronnyy_yashchik:{
                required:  "Введите E-mail",
				email:     "Введите корректный E-mail",
            }, 
			
			parol:{
                required:  "Введите пароль",
				
            }, 			
			
			paroltwo:{
				equalTo:   "Пароль не совпадают",
				
            }, 

		   
       }
	   
		
    });
	
	//Проверка формы регистрации
	$(".update_organizatsiya").validate({
		submitHandler: function(form) {
			
			var m_data=$('.update_organizatsiya').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/update_organizatsiya.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
					$('.update_organizatsiya').slideUp(1000);
					$('.uspeh').html(result.message);
					$('.neuspeh').html('');
				}
				else
				{
					$('.neuspeh').html(result.message);
				}
			}
			});

		},
       rules:{
		   
			iin:{
				required: true,
				digits:true,
				minlength: 12,
                maxlength: 12,	
			},
			
            organizatsiya:{
				required: true,
            },
			
			sfera_deyatelmznosti:{
				required: true,
            },
			
			strana_id:{
				required: true,
            },
			
			regiony_id:{
				required: true,
            },
			
			gorod:{
				required: true,
          
            },
			
			adres:{
				required: true,
            },
			
			familiya:{
				required: true,
            },
			
			kod_mobilmznogo_operatora:{
				required: true,
				digits:true,
				minlength: 3,
                maxlength: 3,	
            },
			
			telefon:{
				required: true,
				digits:true,
				minlength: 7,
                maxlength: 7,				
            },
			
			elektronnyy_yashchik:{
				required: true,
                email:true,
            },
			

       },
	   
       messages:{

		   	organizatsiya:{
                required:  "Введите название организации",
            }, 
			
			sfera_deyatelmznosti:{
                required:  "Введите сферу деятельности",
            }, 

			strana_id:{
                required:  "Выберите страну",
            }, 
			
			regiony_id:{
                required:  "Выберите регион/область",
            }, 

			gorod:{
                required:  "Введите город",
            }, 		

			adres:{
                required:  "Введите адрес",
            }, 	


			familiya:{
                required:  "Введите ФИО контактного лица",
            }, 
			
			kod_mobilmznogo_operatora:{
                required:  "Введите код мобильного оператора, например 777,705,701 и т.д.",
                minlength: "Длина кода не должна быть меньше 3 символов",
                maxlength: "Длина кода не должна быть больше 3 символов",
            }, 
			
			telefon:{
                required:  "Введите телефон контактного лица",
                minlength: "Длина телефона не должна быть меньше 7 символов",
                maxlength: "Длина телефона не должна быть больше 7 символов",
            }, 
			
			elektronnyy_yashchik:{
                required:  "Введите E-mail",
				email:     "Введите корректный E-mail",
            }, 
			
       }
	   
		
    });
	
	
	
	otpravka = 0;
		//Проверка формы регистрации
	$(".register_cena").validate({
		submitHandler: function(form) {
			
			if(otpravka != 1)
						{
						otpravka = 1;
						
						// Показываем наш див
						$('.procces').show();
						
			$.post(
			  "/ajax/sessia_check.php",
			  {
				
			  },
			  function (data)
				{
					if(data == 1)
					{
						
						
						
						
						
						var m_data=$('.register_cena').serialize();
						$.ajax({
						type: 'POST',
						url: '/ajax/register_cena.php',
						data: m_data,
						success: function(result){
							if(result.mailSent == true)
							{
								//alert(result.message);
								//document.location.href = "/";
								$(".filter_postav").load("/ajax/filter_postav.php",{
									zayavka_id: result.zayavka_id});
									
									$('.register_cena')[0].reset();
									
									otpravka = 0;
									//Скрываем наш див
									$('.procces').hide();
									
									$('.procces1').html(result.message);
									
									
							}
							else
							{
								$('.procces1').html(result.message);
							}
						}
						
						});
						
						
						
						
					}
					else
					{
						document.location.href = "/?page=14";
					}
				}
			);
			
			}
						

		},
       rules:{
		   
			cena:{
				required: true,
			
			},
			
          
       },
	   
       messages:{
		   
			cena:{
                required:  "Введите вашу цену за текущий лот",

            }, 
		   
		 		   
       }
	   
		
    });
	
	
		//Проверка формы регистрации
	$(".checkmail").validate({
		submitHandler: function(form) {
			
			var m_data=$('.checkmail').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/checkmail.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
					$('.checkmail').slideUp(1000);
					$('.uspeh').html(result.message);
					$('.neuspeh').html('');
				}
				else
				{
					$('.neuspeh').html(result.message);
				}
			}
			});

		},
       rules:{
		   
		

			email:{
				required: true,
                email:true,
            },
			
		
       },
	   
       messages:{
		   

			email:{
                required:  "Введите E-mail",
				email:     "Введите корректный E-mail",
            }, 
			
				   
       }
	   
		
    });
	
	//Проверка формы регистрации
	$(".update_lot").validate({
		submitHandler: function(form) {
			
			var m_data=$('.update_lot').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/update_lot.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
					$('.update_lot').slideUp(1000);
					$('.uspeh').html(result.message);
					$('.neuspeh').html('');
					//alert(result.message);
				}
				else
				{
					$('.neuspeh').html(result.message);
				}
			}
			});

		},
       rules:{
		   
			kategoriya_lota_id:{
				required: true,
			},
			
            material_v_lote:{
				required: true,
            },
			
			edinitsa_izmereniya_id:{
				required: true,
            },
			
			priobretaemoe_kolichestvo:{
				required: true,
				minlength: 1,
            },
			
			data_otkrytiya_zakupki:{
				required: true,
            },
			
			data_zakrytiya_zakupki:{
				required: true,
            },
			
			mesto_postavki:{
				required: true,
            },

			otvetstvennoe_litso_za_zakupku:{
				required: true,
			},

		
       },
	   
       messages:{
		   
			kategoriya_lota_id:{
                required:  "Выберите категорию лота",
            }, 
		   
		   	material_v_lote:{
                required:  "Введите приобретаемый товар/работу/услугу",
            }, 

			edinitsa_izmereniya_id:{
                required:  "Выберите единицу измерения",
            }, 
			
			priobretaemoe_kolichestvo:{
                required:  "Введите приобретаемое количество товара/работы/услуги",
                minlength: "Длина  не должна быть меньше 1 символа",	
            }, 

			data_otkrytiya_zakupki:{
                required:  "Введите дату публикации лота",
            }, 		

			data_zakrytiya_zakupki:{
                required:  "Введите дату завершения лота",
            }, 	

			mesto_postavki:{
                required:  "Введите место поставки товара/работы/услуги",
            }, 
			
				
			otvetstvennoe_litso_za_zakupku:{
                required:  "Введите ответственное лицо за закупку и его номер телефона",
            }, 

		   
       }
	   
		
    });
	
	
	
	
});